#!/usr/bin/env python3

from PIL import Image
import PIL.ImageOps

# custom python files:
import encrypt
from svgbuilder import SVGBuilder

achannels = [list(i) for i in achannels ]

# WIP
# Generates objects with order of RGB letters from config

with open(imagefile, 'rb') as f:
	image = Image.open( imagefile )
	image.load()

imgsplit = list( image.split() )

clen = len( imgsplit )

cletters = [ "RGBA"[i] for i in range(clen) ]

colorder = cletters[:]

colorMatrix = [ [0] * 5 for i in range(4) ]
# colorMatrix = [ [0] * 5 ] * 4 # avoid; duplicates addresses


colorInvert = [False]*4

for i in range( clen ):
	ci = cletters.index( achannels[ i ][ 0 ] )
	
	colorder[ ci ] = cletters[ i ]
	colorMatrix[ i ][ ci ] = 1
	if '-' in achannels[ i ]:
		colorMatrix[ i ][ 4 ] = 1
		colorMatrix[ i ][ ci ] = -1
		colorInvert[ i ] = True

colorMatrix[3][3] = 1
colorMatrix[3][4] = 0

# merges channel letters list as string 
cjoin = "".join(cletters)

# Builds image

imgsplit = encrypt.inverter( imgsplit, colorInvert )

imgsplit = encrypt.swapper( imgsplit, colorder, cletters )

image = Image.merge( cjoin, imgsplit )

image = encrypt.transform(image,transform.upper())

# Save image and SVG

image.save( imagesave, image.format, quality=95)

if svgsave:
	with open(svgsave, mode='w') as f:
		svg = SVGBuilder( imagesave, image, colorMatrix )
		f.write( svg )
