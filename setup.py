from setuptools import setup, find_packages

setup(name='bitmapremap',
      version='0.4', #'major.minor.patch'
      description='Bitmap data remapper for web images',
      keywords=[ 'image', 'svg', 'color' ],
      author='Squarecrow',
      license='Mozilla Public License 2.0',
      packages=find_packages(),
)
