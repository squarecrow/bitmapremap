#!/usr/bin/env python3

from PIL import Image
import PIL.ImageOps


def inverter( c, inv ):
	for i,b in enumerate(inv):
		if b: c[i] = PIL.ImageOps.invert( c[i] )
	return c

def swapper( img, swap, channels ):
	""" Swaps list items """
	arr = [ channels.index(i) for i in swap ]
	swapped = [ img[i] for i in arr ]
	return swapped

def transformer(image,transformation):
	attr = getattr( Image, transformation )
	transformed = image.transpose( attr )
	return transformed
