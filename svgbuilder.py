#!/usr/bin/env python3

def SVGBuilder(argpath, img, rgba, tmat ):
	
	# Joins rgba matrix list into string
	rgba_str = ' '.join([ str(i) for row in rgba for i in row ])
	
	# Basic transformation matrix list
	# Currently overridden to just horizontally flip the image
	tmat = [-1,0,0,1,img.size[0],0]
	
	# Joins transformation matrix list
	
	trans_str = ','.join( map(str, tmat) )
	
	# SVG template
	with open("template.svg", "r") as f:
		svgtpl = f.read()
	
	return svgtpl.format(
		width		= img.size[0]
		,
		height		= img.size[1]
		,
		image		= argpath
		,
		rgbamatrix	= rgba_str
		,
		tmatrix		= trans_str
	)
