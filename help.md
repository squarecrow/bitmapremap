Bitmap Remapper for SVG
=======================

Avoid using bitmapremap to directly convert png to jpg. Default output jpg quality causes ugly edge color artefacts, when remapped through SVG.

Instead, remap to PNG first, then use IMageMagick to convert remapped png to JPG, and probably also to resize.

```
bitmapremap -f original.png -s remapped.png --channels B,R-,G-,A --transform flip_left_right;
convert remap.png -interlace Plane -resize 1100x400 resized.jpg

```

SVG
---
Color matrix interpolation for inverted colors only works with
"sRGB", not "linearRGB"

Unlike imagemagick, allows channel inversion.

The colorMatrix filter works similarly to the componenTransfer's
"table" and "discrete" types.


python3 bitmapremap -f /home/scarecrow/mydata/web/localhost01.com/public/media/comic/noz-01/m.jpg -s /home/scarecrow/mydata/web/localhost01.com/public/media/comic/noz-01/m2.jpg --channels B,R-,G-,A --transform flip_left_right


USAGE CONCEPT
-------------


for each i in listcount:
 python main.py -f images[i] -m matrix[i] -o images2[i] -v svgs[i]
for i in images2[i]:
 iMedium
 iSmall
 iThumbnail
